<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 11/13/21
  Time: 9:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="../../shared/head.jsp"></jsp:include>

    <title>Shoes Store - Thương hiệu</title>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <jsp:include page="../_leftbar.jsp"></jsp:include>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <jsp:include page="../_search.jsp"></jsp:include>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <div>
                    <a href="${pageContext.request.contextPath}/manager/brand/new" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-flag"></i>
                            </span>
                        <span class="text">Thêm thương hiệu</span>
                    </a>
                </div>
                <br>
                <c:if test="${not empty msg}">
                    <div>
                        <p class="text-success">
                            <span class="text">${msg}</span>
                        </p>
                    </div>
                </c:if>
                <c:if test="${not empty msg_err}">
                    <div>
                        <p class="text-danger">
                            <span class="text">${msg_err}</span>
                        </p>
                    </div>
                </c:if>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Danh sách thương hiệu</h6>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Tên thương hiệu</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listBrand}" var="brand">
                                    <tr>
                                            <td>${brand.id}</td>
                                            <td>${brand.name}</td>
                                            <td>
                                                <a class="btn btn-success" href="${pageContext.request.contextPath}/manager/brand/edit?id=${brand.id}">Sửa</a>
                                                <a class="btn btn-danger" onclick="return confirm('Bạn muốn xóa thương hiệu này');" href="${pageContext.request.contextPath}/manager/brand/delete?id=${brand.id}">Xóa</a>
                                            </td>
                                    </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <jsp:include page="../_footer.jsp"></jsp:include>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById("brandActive").classList.add('active');
</script>

<jsp:include page="../../shared/script.jsp"></jsp:include>

</body>

</html>