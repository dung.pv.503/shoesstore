<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12/21/2021
  Time: 10:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<!--===== header =====-->

<jsp:include page="./_header.jsp"></jsp:include>

<!--===== page-title =====-->

<section class="page-title bg-overlay-black parallax page-title-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Thay đổi thông tin</h1>
            </div>
        </div>
    </div>
</section>

<!--===== End page-title =====-->

<!--===== End header =====-->
<section class="page-section-ptb">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-block">
                    <c:if test="${not empty msg}">
                        <div>
                            <p class="text-success">
                                <span class="text">${msg}</span>
                            </p>
                        </div>
                    </c:if>
                    <c:if test="${not empty err}">
                        <div>
                            <p class="text-danger">
                                <span class="text">${err}</span>
                            </p>
                        </div>
                    </c:if>
                    <div class="login-form">
<s:form method="POST" modelAttribute="user" action="${pageContext.request.contextPath}/thong-tin">
    <s:hidden path="id" class="form-control" />

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                Họ tên
                <s:input path="userName" class="form-control"  />

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Số điện thoại
                <s:input path="phoneNumber"  class="form-control" />

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Địa chỉ
                <s:input path="address" class="form-control" />

            </div>
        </div>
    </div>
    <button class="btn theme-button animated slideInRight" type="submit">Cập nhật thông tin</button>
</s:form>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=====End Add Banner Section =====-->

<!--===== Footer =====-->
<jsp:include page="./_footer.jsp"></jsp:include>
<!--=====End Footer =====-->
