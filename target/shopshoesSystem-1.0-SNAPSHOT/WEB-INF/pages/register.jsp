<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="shared/head.jsp"></jsp:include>
    <title>Shoes Store - Đăng ký tài khoản</title>
    <style>
        .error{
            padding-top: 5px;
            font-size: 12px;
            color: red;
        }
        input{
            display: block;
        }
    </style>
</head>

<body class="bg-gradient-primary">

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Đăng ký tài khoản</h1>
                                </div>
                                <c:if test="${not empty err}">
                                    <p class="text-danger">${err}</p>
                                </c:if>
                                <form id="register" action="${pageContext.request.contextPath}/dang-ky" method="post" class="user">

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="text" name="name" class="form-control form-control-user"
                                                   id="exampleInputUserName" placeholder="Họ và tên">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="email" class="form-control form-control-user"
                                                   id="exampleRepeatEmail" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="password" name="password" class="form-control form-control-user"
                                                   id="exampleInputPassword" placeholder="Mật khẩu">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="password" name="repassword" class="form-control form-control-user"
                                                   id="exampleRepeatPassword" placeholder="Nhập mật khẩu xác nhận">
                                        </div>
                                    </div>
                                    <button type="submit" onclick="submitForm();" class="btn btn-primary btn-user btn-block">
                                        Đăng ký
                                    </button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    Bạn đã có tài khoản ?<a class="small" href="${pageContext.request.contextPath}/dang-nhap"> Đăng nhập ngay</a>
                                </div>
                                <br>
                                <div class="text-center">
                                    <a class="small" href="${pageContext.request.contextPath}/trang-chu">< Trở về trang chủ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="shared/script.jsp"></jsp:include>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
</body>



<script type="text/javascript">

    function submitForm()
    {
        $("#register").validate({
            rules:
                {
                    "name":
                        {
                            required: true
                        },
                    "email":{
                        required: true,
                        email: true
                    },
                    "password":{
                        required:true,
                        minlength: 6,
                        maxlength: 30,
                    },
                    "repassword":{
                        required:true,
                        minlength: 6,
                        maxlength: 30,
                        equalTo: '[name="password"]'
                    }
                },
            messages:
                {
                    "name":
                        {
                            required: "Vui lòng nhập họ và tên"
                        },
                    "email":{
                        required: "Vui lòng nhập email",
                        email: "Vui lòng nhập đúng định dạng email"
                    },
                    "password":{
                        required:"Vui lòng nhập mật khẩu",
                        minlength: "Mật khẩu phải có ít nhất 6 kí tự trở lên",
                        maxlength: "Mật khẩu có tối đa 30 kí tự",
                    },
                    "repassword":{
                        required: "Vui lòng nhập mật khẩu xác nhận",
                        minlength: "Mật khẩu xác nhận phải có ít nhất 6 kí tự trở lên",
                        maxlength: "Mật khẩu xác nhận có tối đa 30 kí tự",
                        equalTo: "Mật khẩu xác nhận và mật khẩu không khớp"
                    }
                }
        });
    }
</script>

</html>