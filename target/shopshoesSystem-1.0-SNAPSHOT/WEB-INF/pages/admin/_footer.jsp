<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12/8/2021
  Time: 9:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <p> ©Copyright 2021 <a href="${pageContext.request.contextPath}/trang-chu"> Shoes Store </a> All Rights Reserved </p>
        </div>
    </div>
</footer>
