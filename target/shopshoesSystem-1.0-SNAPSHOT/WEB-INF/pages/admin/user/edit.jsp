<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 11/13/21
  Time: 9:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="../../shared/head.jsp"></jsp:include>

    <title>Shoes Store - Cập nhật tài khoản</title>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <jsp:include page="../_leftbar.jsp"></jsp:include>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <jsp:include page="../_search.jsp"></jsp:include>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <div class="col-md-7 col-lg-8">
                    <h4 class="mb-3">Cập nhật tài khoản</h4>

                    <s:form method="POST" modelAttribute="user" action="${pageContext.request.contextPath}/manager/user/edit">
                        <!-- input text code -->
                        <s:hidden path="id" class="form-control" />
                        <!-- input text code-->
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Địa chỉ email</span>
                                </div>
                                <s:input path="email" class="form-control" />
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Mật khẩu</span>
                                </div>
                                <input type="password" name="password" value="<c:out value="${password}"/>" class="form-control">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Số điện thoại</span>
                                </div>
                                <s:input path="phoneNumber"  class="form-control" />
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Họ và tên</span>
                                </div>
                                <s:input path="userName" class="form-control" />
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Loại tài khoản</span>
                                </div>
                                <s:input path="userType" class="form-control" />
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Địa chỉ</span>
                                </div>
                                <s:input path="address" class="form-control" />
                            </div>
                        </div>
                        <a class="btn btn-danger" href="${pageContext.request.contextPath}/manager/user">Quay về</a>
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </s:form>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <jsp:include page="../_footer.jsp"></jsp:include>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById("userActive").classList.add('active');
</script>

<jsp:include page="../../shared/script.jsp"></jsp:include>


</body>

</html>